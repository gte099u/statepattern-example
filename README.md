**Welcome to the State Pattern Example project**

# Overview
This project uses the following workflow:

```[Draft] -> [Submitted] -> [Reviewed]  -> [Approved] -> [Paid] -> [Reconciled]```

To run the sample console app, be sure that the console project is set as the Startup Project, and run it with F5.

# Exercise


1. A new requirement is introduced that *Submitted* invoices must be reviewed prior to approval.
   * Please add a new *Reviewed* status between *Submitted* and *Approved* in the workflow.  *Reviewed* invoices may **not** be canceled.
2. A new requirement is introduced that *Reviewed* invoiced may be contested.  *Contested* invoices are set back to the business and must be redrafted.
   * Please add a new *Contested* state that may be set from the *Reviewed* state
	
   * *Contested* invoices may move to *Draft*, or may be *Canceled*

3. We need an audit trail of who approved an invoice.
   * Please update the workflow to capture the userId (string) of the approver