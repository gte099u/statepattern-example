﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StatePatternExample
{
    public class DataConstants
    {
        public const string INVOICE_STATUS_DRAFT = "DRAFT";
        public const string INVOICE_STATUS_SUBMITTED = "SUBMITTED";
        public const string INVOICE_STATUS_APPROVED = "APPROVED";
        public const string INVOICE_STATUS_PAID = "PAID";
        public const string INVOICE_STATUS_RECONCILED = "RECONCILED";
        public const string INVOICE_STATUS_CANCELED = "CANCELED";
    }
}
