﻿using System;

namespace StatePatternExample
{
    public class WorkflowStateCanceled : WorkflowStateBase
    {
        public WorkflowStateCanceled(Invoice invoice) : base(invoice) { }

        public override string CurrentState
        {
            get { return DataConstants.INVOICE_STATUS_CANCELED; }
        }

        // Canceled invoices and may have no further action taken
        // removing all overrides here prevents any state changes
    }
}
