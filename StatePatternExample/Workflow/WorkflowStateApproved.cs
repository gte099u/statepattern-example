﻿using System;

namespace StatePatternExample
{
    public class WorkflowStateApproved : WorkflowStateBase
    {
        public WorkflowStateApproved(Invoice invoice) : base(invoice) { }

        public override string CurrentState
        {
            get { return DataConstants.INVOICE_STATUS_APPROVED; }
        }

        public override void SetAsPaid(string paymentConfirmationCode)
        {
            // validate, log, or persist the confirmation code as necessary
            Invoice.Status = DataConstants.INVOICE_STATUS_PAID;
        }

        // we've removed the override of Cancel which prevents invoices in this state from being canceled
    }
}
