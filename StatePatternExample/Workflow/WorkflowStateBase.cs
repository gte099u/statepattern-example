﻿using System;
using System.Linq;

namespace StatePatternExample
{
    public abstract class WorkflowStateBase : IWorkflowState
    {
        protected readonly Invoice Invoice;

        protected WorkflowStateBase(Invoice invoice)
        {
            Invoice = invoice;
        }

        public abstract string CurrentState { get; }

        public virtual void Cancel()
        {
            throw new InvalidOperationException();
        }

        public virtual void SetAsApproved()
        {
            throw new InvalidOperationException();
        }

        public virtual void SetAsDraft()
        {
            throw new InvalidOperationException();
        }

        public virtual void SetAsPaid(string paymentConfirmationCode)
        {
            throw new InvalidOperationException();
        }

        public virtual void SetAsReconciled()
        {
            throw new InvalidOperationException();
        }

        public virtual void SetAsSubmited()
        {
            throw new InvalidOperationException();
        }
    }
}
