﻿using System;

namespace StatePatternExample
{
    public class WorkflowStateDraft : WorkflowStateBase
    {
        public WorkflowStateDraft(Invoice invoice) : base(invoice) { }

        public override string CurrentState
        {
            get { return DataConstants.INVOICE_STATUS_DRAFT; }
        }

        public override void SetAsSubmited()
        {
            // perform any validation to ensure that the invoice is ready for submission
            if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                throw new Exception("Invoices may not be submitted on Sundays");

            Invoice.Status = DataConstants.INVOICE_STATUS_SUBMITTED;
        }

        public override void Cancel()
        {
            // trigger any other process that must occur when an invoice is canceled
            Invoice.Status = DataConstants.INVOICE_STATUS_CANCELED;
        }
    }
}
