﻿using System;

namespace StatePatternExample
{
    public interface IWorkflowState
    {        
        string CurrentState { get; }                
        void SetAsDraft();
        void SetAsSubmited();
        void SetAsApproved();
        void SetAsPaid(string paymentConfirmationCode);
        void SetAsReconciled();
        void Cancel();
    }
}
