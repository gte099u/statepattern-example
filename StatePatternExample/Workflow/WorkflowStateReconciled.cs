﻿using System;

namespace StatePatternExample
{
    public class WorkflowStateReconciled : WorkflowStateBase
    {
        public WorkflowStateReconciled(Invoice invoice) : base(invoice) { }

        public override string CurrentState
        {
            get { return DataConstants.INVOICE_STATUS_RECONCILED; }
        }

        // Reconciled invoices have completed the workflow and may have no further action taken
        // removing all overrides here prevents any state changes
    }
}
