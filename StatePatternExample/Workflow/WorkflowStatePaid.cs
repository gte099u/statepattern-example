﻿using System;

namespace StatePatternExample
{
    public class WorkflowStatePaid : WorkflowStateBase
    {
        public WorkflowStatePaid(Invoice invoice) : base(invoice) { }

        public override string CurrentState
        {
            get { return DataConstants.INVOICE_STATUS_PAID; }
        }

        public override void SetAsReconciled()
        {
            Invoice.Status = DataConstants.INVOICE_STATUS_RECONCILED;
        }
    }
}
