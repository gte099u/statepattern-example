﻿using System;

namespace StatePatternExample
{
    public class WorkflowStateSubmitted : WorkflowStateBase
    {
        public WorkflowStateSubmitted(Invoice invoice) : base(invoice) { }

        public override string CurrentState
        {
            get { return DataConstants.INVOICE_STATUS_SUBMITTED; }
        }

        public override void SetAsApproved()
        {
            Invoice.Status = DataConstants.INVOICE_STATUS_APPROVED;
        }

        public override void Cancel()
        {
            // trigger any other process that must occur when an invoice is canceled
            Invoice.Status = DataConstants.INVOICE_STATUS_CANCELED;
        }
    }
}
