﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StatePatternExample
{
    public class InvoiceFactory
    {
        public Invoice Create()
        {
            return new Invoice
            {
                Status = DataConstants.INVOICE_STATUS_DRAFT
            };
        }
    }
}
