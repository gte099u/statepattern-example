﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StatePatternExample
{
    public partial class Invoice : IWorkflowState
    {
        // this property is stored in the DB as a varchar
        public string Status { get; set; }
        public string PaymentConfirmationCode { get; set; }

        private IWorkflowState State
        {
            get
            {
                if (string.IsNullOrEmpty(Status))
                    return null;

                switch (Status)
                {
                    case DataConstants.INVOICE_STATUS_DRAFT:
                        return new WorkflowStateDraft(this);
                    case DataConstants.INVOICE_STATUS_SUBMITTED:
                        return new WorkflowStateSubmitted(this);
                    case DataConstants.INVOICE_STATUS_APPROVED:
                        return new WorkflowStateApproved(this);
                    case DataConstants.INVOICE_STATUS_PAID:
                        return new WorkflowStatePaid(this);
                    case DataConstants.INVOICE_STATUS_RECONCILED:
                        return new WorkflowStateReconciled(this);
                    case DataConstants.INVOICE_STATUS_CANCELED:
                        return new WorkflowStateCanceled(this);
                    default:
                        throw new Exception("Unknown invoice status");
                }
            }
        }

        public string CurrentState
        {
            get { return State.CurrentState; }
        }

        public void SetAsDraft()
        {
            State.SetAsDraft();
        }

        public void SetAsSubmited()
        {
            State.SetAsSubmited();
        }

        public void SetAsApproved()
        {
            State.SetAsApproved();
        }

        public void SetAsPaid(string paymentConfirmationCode)
        {
            State.SetAsPaid(paymentConfirmationCode);
        }

        public void SetAsReconciled()
        {
            State.SetAsReconciled();
        }


        public void Cancel()
        {
            State.Cancel();
        }

        public override string ToString()
        {
            return $"Invoice Status: {Status}{(!string.IsNullOrEmpty(PaymentConfirmationCode) ? $"; Confirmation Code: {PaymentConfirmationCode}" : "")}";
        }
    }
}
