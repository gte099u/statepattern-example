﻿using System;

namespace StatePatternExample.ConsoleApp
{
    class Program
    {
        public static Invoice Invoice { get; set; }
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the state pattern sample app");
            Console.WriteLine("Let's beging by creating a new invoice - press any key to continue");
            Console.ReadLine();
            var factory = new InvoiceFactory();
            Invoice = factory.Create();
            Console.WriteLine("We have the following invoice ->");
            Console.WriteLine(Invoice);

            TakeUserInput();
        }

        private static void TakeUserInput()
        {
            Console.WriteLine("Please choose from the following options:");
            Console.WriteLine("[S]ubmit, [A]pprove, [P]ay, [R]econcile, [C]ancel, [E]xit");
            var response = Console.ReadLine();

            try
            {
                switch (response)
                {
                    case "S":
                        {
                            Invoice.SetAsSubmited();
                            Console.WriteLine("Invoice submitted");
                            Console.WriteLine(Invoice);
                        }
                        break;
                    case "A":
                        {
                            Invoice.SetAsApproved();
                            Console.WriteLine("Invoice approved");
                            Console.WriteLine(Invoice);
                        }
                        break;
                    case "P":
                        {
                            Console.WriteLine("Please enter a payment confirmation code");
                            var code = Console.ReadLine();
                            Invoice.SetAsPaid(code);
                            Console.WriteLine("Invoice paid");
                            Console.WriteLine(Invoice);
                        }
                        break;
                    case "R":
                        {
                            Invoice.SetAsReconciled();
                            Console.WriteLine("Invoice reconciled");
                            Console.WriteLine(Invoice);
                        }
                        break;
                    case "C":
                        {
                            Invoice.Cancel();
                            Console.WriteLine("Invoice canceled");
                            Console.WriteLine(Invoice);
                        }
                        break;
                    case "E":
                        {
                            Console.WriteLine("Thank you for playing!");
                        }
                        break;
                    default:
                        {
                            Console.WriteLine("Invalid input, please try again");
                        }
                        break;
                }
                if (response != "E")
                {
                    TakeUserInput();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("!! That action is not permitted for the current invoice state !!");
                TakeUserInput();
            }
        }
    }
}
